import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { deleteData, detailData, getDataList } from '../../actions/dataAction';

const Data = () => {

  const {getDataResult, getDataLoading, getDataError, deleteDataResult} = useSelector((state)=>{
    return state.DataReducer
  })
  const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(getDataList());
  },[dispatch])

  useEffect(()=>{
    if(deleteDataResult){
      console.log("load saat delete");
      dispatch(getDataList());
    }
  },[deleteDataResult, dispatch]);
  
  return (
    <>
    {
      getDataResult?(
        getDataResult.map((data)=>{
          return (
          <div className="block is-clickable" key={data.id}>
            <span className="tag is-success is-large" onClick={()=>dispatch(detailData(data))}>
              {data.name}
            </span>
            <button onClick={()=>dispatch(deleteData(data.id))} className="delete is-large" />
          </div>
          )
        })
      ):(
        <p>Loading...</p>
      )
    }
    </>
  )
}

export default Data