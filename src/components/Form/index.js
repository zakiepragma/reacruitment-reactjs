import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { applyData, getDataList, updateData } from '../../actions/dataAction';

const Form = () => {

  const [name,setName] = useState("");
  const [email, setEmail] = useState("");
  const [salary, setSalary] = useState("");
  const [message, setMessage] = useState("");
  const [id, setId] = useState("");
  const [button, setButton] = useState("Apply");
  const [nameError, setNameError] = useState("");
  const [emailError, setEmailError] = useState("");

  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    if(name){
      if(email){
        if(id){
          dispatch(updateData({id:id, name:name,email:email,salary:salary,message:message}))
        }else{
          dispatch(applyData({name:name,email:email,salary:salary,message:message}))
        }
      }else{
        setEmailError("E-Mail required");
      }
    }else{
      setNameError("Name required");
    }
  }

  const {applyDataResult, detailDataResult, updateDataResult} = useSelector((state)=> { return state.DataReducer});

  useEffect(()=>{
    setNameError("");
    setEmailError("");
    if(applyDataResult){
      console.log("load saat submit")
      dispatch(getDataList());
      setName("");
      setEmail("");
      setSalary("");
      setMessage("");
    }
  },[applyDataResult, dispatch]);

  useEffect(()=>{
    setNameError("");
    setEmailError("");
    if(detailDataResult){
      console.log("load saat klik detail")
      setName(detailDataResult.name);
      setEmail(detailDataResult.email);
      setSalary(detailDataResult.salary);
      setMessage(detailDataResult.message);
      setId(detailDataResult.id);
      setButton("Update");
      setNameError("");
      setEmailError("");
    }
  },[detailDataResult, dispatch]);

  useEffect(()=>{
    setNameError("");
    setEmailError("");
    if(updateDataResult){
      console.log("load saat update")
      dispatch(getDataList());
      setName("");
      setEmail("");
      setSalary("");
      setMessage("");
      setButton("Apply");
      setNameError("");
      setEmailError("");
      setId("");
    }
  },[updateDataResult, dispatch]);

  const reset  = () =>{
      setName("");
      setEmail("");
      setSalary("");
      setMessage("");
      setButton("Apply");
      setNameError("");
      setEmailError("");
      setId("");
  }

  return (
    <div className='box'>
      <form onSubmit={(event)=>handleSubmit(event)}>
      <div className="field">
        <label className="label">Name</label>
        <div className="control">
          <input className="input is-success" type="text" placeholder="Name" value={name} onChange={(event)=>setName(event.target.value)}/>
        </div>
        <p className="help is-danger">{nameError}</p>
      </div>
      <div className="field">
        <label className="label">E-Mail</label>
        <div className="control">
          <input className="input is-success" type="text" placeholder="E-Mail" value={email} onChange={(event)=>setEmail(event.target.value)}/>
        </div>
        <p className="help is-danger">{emailError}</p>
      </div>
      <div className="field">
        <label className="label">Expected Salary</label>
        <div className="control">
          <input className="input is-success" type="text" placeholder="Expected Salary" value={salary} onChange={(event)=>setSalary(event.target.value)}/>
        </div>
      </div>
      <div className="field">
        <label className="label">Message</label>
        <div className="control">
          <textarea className="textarea" placeholder="Message" onChange={(event)=>setMessage(event.target.value)} value={message}/>
        </div>
      </div>
      <div className="field is-grouped">
        <div className="control">
          <button type='submit' className="button is-link">{button}</button>
        </div>
        <div className="control">
          <button className="button is-link is-light" onClick={()=>reset()}>Reset</button>
        </div>
      </div>
      </form>
    </div>
  )
}

export default Form