import React from 'react'

function Navbar() {
  return (
    <nav className="navbar is-transparent">
        <div className="navbar-brand">
            <a className="navbar-item" href="">
                <p className='title is-4"'>Recruitment App</p>
            </a>
        </div>
        <div className="navbar-end">
            <div className="navbar-item">
                <div className="field is-grouped">
                    <p className="control">
                        <a className="button is-warning" target="_blank" href="https://gitlab.com/zakiepragma/reacruitment-reactjs">
                        <span>Download Source Code</span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </nav>
  )
}

export default Navbar