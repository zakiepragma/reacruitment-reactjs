import axios from "axios";

export const GET_DATA_LIST = "GET_DATA_LIST";
export const APPLY_DATA = "APPLY_DATA";
export const DELETE_DATA = "DELETE_DATA";
export const DETAIL_DATA = "DETAIL_DATA";
export const UPDATE_DATA = "UPDATE_DATA";

export const getDataList = () =>{
    return (dispatch) => {
        dispatch({
            type: GET_DATA_LIST,
            payload:{
                loading:true,
                data:false,
                errorMessage:false
            }
        })

        axios({
            method:'GET',
            url:'http://localhost:3000/recruitment',
            timeout:120000
        }).then((response)=>{
            dispatch({
                type: GET_DATA_LIST,
                payload:{
                    loading:false,
                    data:response.data,
                    errorMessage:false
                }
            })
        }).catch((error)=>{
            dispatch({
                type: GET_DATA_LIST,
                payload:{
                    loading:false,
                    data:false,
                    errorMessage:error.message
                }
            })
        })
    }
}

export const applyData = (data) =>{
    return (dispatch) => {
        dispatch({
            type: APPLY_DATA,
            payload:{
                loading:true,
                data:false,
                errorMessage:false
            }
        })

        axios({
            method:'POST',
            url:'http://localhost:3000/recruitment',
            timeout:120000,
            data:data
        }).then((response)=>{
            dispatch({
                type: APPLY_DATA,
                payload:{
                    loading:false,
                    data:response.data,
                    errorMessage:false
                }
            })
        }).catch((error)=>{
            dispatch({
                type: APPLY_DATA,
                payload:{
                    loading:false,
                    data:false,
                    errorMessage:error.message
                }
            })
        })
    }
}

export const deleteData = (id) =>{
    return (dispatch) => {
        dispatch({
            type: DELETE_DATA,
            payload:{
                loading:true,
                data:false,
                errorMessage:false
            }
        })

        axios({
            method:'DELETE',
            url:'http://localhost:3000/recruitment/'+id,
            timeout:120000,
        }).then((response)=>{
            dispatch({
                type: DELETE_DATA,
                payload:{
                    loading:false,
                    data:response.data,
                    errorMessage:false
                }
            })
        }).catch((error)=>{
            dispatch({
                type: DELETE_DATA,
                payload:{
                    loading:false,
                    data:false,
                    errorMessage:error.message
                }
            })
        })
    }
}

export const detailData = (data) =>{
    return (dispatch) => {
        dispatch({
            type: DETAIL_DATA,
            payload:{
                data:data,
            }
        })
    }
}

export const updateData = (data) =>{
    return (dispatch) => {
        dispatch({
            type: UPDATE_DATA,
            payload:{
                loading:true,
                data:false,
                errorMessage:false
            }
        })

        axios({
            method:'PUT',
            url:'http://localhost:3000/recruitment/'+data.id,
            timeout:120000,
            data:data
        }).then((response)=>{
            dispatch({
                type: UPDATE_DATA,
                payload:{
                    loading:false,
                    data:response.data,
                    errorMessage:false
                }
            })
        }).catch((error)=>{
            dispatch({
                type: UPDATE_DATA,
                payload:{
                    loading:false,
                    data:false,
                    errorMessage:error.message
                }
            })
        })
    }
}