import { APPLY_DATA, DELETE_DATA, DETAIL_DATA, GET_DATA_LIST, UPDATE_DATA } from "../../actions/dataAction";

const initialState = {
    getDataResult: false,
    getDataLoading: false,
    getDataError: false,

    applyDataResult: false,
    applyDataLoading: false,
    applyDataError: false,

    deleteDataResult: false,
    deleteDataLoading: false,
    deleteDataError: false,

    detailDataResult: false,

    updateDataResult: false,
    updateDataLoading: false,
    updateDataError: false,
}

const data = (state = initialState, action) => {
    switch(action.type){
        case GET_DATA_LIST:
            return {
                ...state,
                getDataResult: action.payload.data,
                getDataLoading: action.payload.loading,
                getDataError: action.payload.error
            }
        case APPLY_DATA:
            return {
                ...state,
                applyDataResult: action.payload.data,
                applyDataLoading: action.payload.loading,
                applyDataError: action.payload.error
            }
        case DELETE_DATA:
            return {
                ...state,
                deleteDataResult: action.payload.data,
                deleteDataLoading: action.payload.loading,
                deleteDataError: action.payload.error
            }
        case DETAIL_DATA:
            return {
                ...state,
                detailDataResult: action.payload.data,
            }
        case UPDATE_DATA:
            return {
                ...state,
                updateDataResult: action.payload.data,
                updateDataLoading: action.payload.loading,
                updateDataError: action.payload.error
            }
        default:
            return state;
    }
}

export default data