import Data from "./components/Data";
import Form from "./components/Form";
import Navbar from "./components/Navbar";

function App() {
  return (
    <div className="container">
    <Navbar/>
        <div className="columns is-desktop mt-3">
          <div className="column">
            <Form/>
          </div>
          <div className="column">
            <Data/>
          </div>
        </div>
    </div>
  );
}

export default App;
